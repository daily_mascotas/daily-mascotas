import psycopg2


class Conexion_DB:
    def __init__(self, mi_host, mi_database, mi_user, mi_passwd):
        try:
            # fruto de la conexion se crea e objeto conn como atributo
            self.conn = psycopg2.connect(
                host=mi_host,
                database=mi_database,
                user=mi_user,
                password=mi_passwd)  # conectando a db con las credenciales dadas en parametros
            # del constructor

            # create a cursor
            self.cur = self.conn.cursor()  # se crea el objeto cur como atributo
        except:
            print("Conexion no lograda")

    def consultar_db(self, query):
        # metodo usado para ejecutar queries que retornan informacion
        # (SELECT), la query a ejecutar se pasa como parametro al metodo
        try:
            self.cur.execute(query)  # ejecutamos la query con el atributo cur
            response = self.cur.fetchall()  # leemos lo que la query retorna
            return response
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return "error"

    def escribir_db(self, query):
        # metodo usado para queries que no retornan datos
        # queries de escritura -> INSERT,DELETE,CREATE
        try:
            self.cur.execute(query)  # ejecutamos la query
            self.conn.commit()  # actualizamos las tablas
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return "error"
    
    def cerrar_db(self):
        self.cur.close()
        self.conn.close()
        self.conn.close()

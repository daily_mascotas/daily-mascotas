from typing import final
import psycopg2

conexion = psycopg2.connect(
    user='postgres',
    password='admin',
    host='127.0.0.1',
    port='5432',
    database='postgres')

""" try:
    with conexion:
        with conexion.cursor() as cursor:
            sentencia = 'SELECT * FROM usuarios WHERE id_usuario IN %s'
            #llaves_primarias = ((1, 2, 3),)
            #id_usuario = input('Proporciona el valor de id_usuario')
            #Se le solicita al usuario que ingrese los valores separados por comas
            entrada = input('Proporciona los id\'s a buscar (separado por comas): ')
            #se convierte en tupla y se eliminan las comas trayendo solo los números
            llaves_primarias = (tuple(entrada.split(',')),)
            cursor.execute(sentencia, llaves_primarias)
            registros = cursor.fetchall()
            for registro in registros:
                print(registro)

except Exception as e:
    print(f'Ocurrio un error: {e}')
finally:
    conexion.close() """

try:
    with conexion:
        with conexion.cursor() as cursor:
            sentencia = 'INSERT INTO usuarios(nombre, apellidos, cedula, celular, mail, contrasena) VALUES (%s, %s, %s, %s, %s, %s)'
            valores = ('Alejandra', 'Rodriguez', '1000345876', '315987345', 'arodriguez@correo.com', 'pass')
            cursor.execute(sentencia, valores)
            registros_insertados = cursor.rowcount
            print(f'Registros Insertados: {registros_insertados}')

except Exception as e:
    print(f'Ocurrio un error: {e}')
finally:
    conexion.close()


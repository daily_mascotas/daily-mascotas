from flask import Flask, request, make_response, redirect,render_template
from flask.wrappers import Response  # clase Flask de extension flask

app = Flask(__name__)  # instancia de Flask
from Crud import Crud
import json  # para poder dar formato json
from datetime import date, datetime

# consulta usuarios:
@app.route("/get_usuarios")
def leer_usuarios():
    crud = Crud("ec2-3-219-111-26.compute-1.amazonaws.com", "d4j9tqed1qf21a", "uphpbguryimaoc", "b61757b8fd94533331a6c7a0f1ed9a58704b5ce995434cc046c7a8f4decd0ae7")
    usuarios = crud.leer_usuarios()
    respuesta = []
    for registro in usuarios:
        respuesta.append({"id_usuario": registro[0], "nombre": registro[1],
                          "apellidos": registro[2], "cedula": registro[3], "celular": registro[4],
                           "mail": registro[5], "contrasena": registro[6]})

    respuesta = json.dumps(respuesta)
    return render_template("list_usuarios.html",usuarios=usuarios)

@app.route("/")
def login():
    return render_template("login.html")

@app.route("/login", methods=["POST"])
def iniciar_sesion():
     if request.method == 'POST':
        user=request.form['user']
        password=request.form['password']
        crud = Crud("ec2-3-219-111-26.compute-1.amazonaws.com", "d4j9tqed1qf21a", "uphpbguryimaoc", "b61757b8fd94533331a6c7a0f1ed9a58704b5ce995434cc046c7a8f4decd0ae7")
        resultado= crud.login(user,password)
        print(resultado)
        if resultado== None or resultado== []:
            return render_template("login.html", message="Usuario o clave invalidos")
        else:
            return redirect("/index")

@app.route("/get_usuario/<int:id_usuario>")
def get_usuario(id_usuario=0):
    crud = Crud("ec2-3-219-111-26.compute-1.amazonaws.com", "d4j9tqed1qf21a", "uphpbguryimaoc", "b61757b8fd94533331a6c7a0f1ed9a58704b5ce995434cc046c7a8f4decd0ae7")
    usuario = crud.leer_usuario(id_usuario)
    filtro_usuario = usuario[0]
    # convertir a string
    print(usuario)
    link="/editar_usuario/"+str(id_usuario)

    retorno = json.dumps({"id": filtro_usuario[0], "nombre": filtro_usuario[1],
                          "apellidos": filtro_usuario[2], "cedula": filtro_usuario[3], "celular": filtro_usuario[4],
                          "mail": filtro_usuario[5], "contrasena": filtro_usuario[6]})
    return render_template("usuario.html",usuario=usuario[0],link=link)

@app.route("/eliminar_usuario/<int:id_usuario>")
def eliminar_usuario(id_usuario=0):
    crud = Crud("ec2-3-219-111-26.compute-1.amazonaws.com", "d4j9tqed1qf21a", "uphpbguryimaoc", "b61757b8fd94533331a6c7a0f1ed9a58704b5ce995434cc046c7a8f4decd0ae7")
    crud.eliminar_usuario(id_usuario)
    return redirect("/get_usuarios")

@app.route("/create_usuario")
def crear_usuario():
    crud = Crud("ec2-3-219-111-26.compute-1.amazonaws.com", "d4j9tqed1qf21a", "uphpbguryimaoc", "b61757b8fd94533331a6c7a0f1ed9a58704b5ce995434cc046c7a8f4decd0ae7")
    link="/insertar_usuario"
    return render_template("usuario.html",usuario=None,link=link)

@app.route("/insertar_usuario", methods=["POST"])
def insertar_usuario():
    if request.method == 'POST':
        nombres = request.form['nombres']
        apellidos = request.form['apellidos']
        cedula = request.form['cedula']
        mail=request.form['correo']
        celular=request.form['celular']
        clave=request.form['clave']
        crud = Crud("ec2-3-219-111-26.compute-1.amazonaws.com", "d4j9tqed1qf21a", "uphpbguryimaoc", "b61757b8fd94533331a6c7a0f1ed9a58704b5ce995434cc046c7a8f4decd0ae7")
        crud.insertar_usuario(nombres,apellidos,cedula,celular,mail,clave)
        return redirect("/get_usuarios")

@app.route("/editar_usuario/<int:id>", methods=["POST"])
def editar_usuario(id=0):
    if request.method == 'POST':
        nombres = request.form['nombres']
        apellidos = request.form['apellidos']
        cedula = request.form['cedula']
        mail=request.form['correo']
        celular=request.form['celular']
        clave=request.form['clave']
        crud = Crud("ec2-3-219-111-26.compute-1.amazonaws.com", "d4j9tqed1qf21a", "uphpbguryimaoc", "b61757b8fd94533331a6c7a0f1ed9a58704b5ce995434cc046c7a8f4decd0ae7")
        crud.modificar_usuario(str(id),nombres,apellidos,cedula,celular,mail,clave)
        return redirect("/get_usuarios")

@app.route("/pqr")
def pqr():
    return render_template("pqr.html")

@app.route("/construccion")
def construccion():
    return render_template("construccion.html")

@app.route("/ventas")
def ventas():
    return render_template("ventas.html")

@app.route("/index")
def home():
    return render_template("index.html")

if __name__ == "__main__":  # punto de partida, donde python empieza a ejecutar
    while (True):
        print("starting web server")
        app.run(debug=True, host='0.0.0.0')  # arranca el servidor, 0.0.0.0->localhost
    # en el navegador se accede localhost:5000/

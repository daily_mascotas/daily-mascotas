from Conexion_DB import Conexion_DB


class Crud:
    def __init__(self, host, db, user, passwd):
        self.my_conn = Conexion_DB(host, db, user, passwd)
        # creando un objeto de la clase conexion_db y definiendolo
        # como atributo de la clase Crud (self)

    # escribir metodos para escribir, leer, eliminar,actualizar registros de tablas
    # segun las necesidades de mi proyecto

    def leer_usuarios(self):
        query = "SELECT id_usuario, " + \
                " nombre, " + \
                " apellidos, " + \
                " cedula, " + \
                " celular, " + \
                " mail, " + \
                " contrasena" + \
                " FROM public.usuarios; "
        respuesta = self.my_conn.consultar_db(query)
        return respuesta

    def leer_usuario(self, id):
        query = "SELECT id_usuario, " + \
                " nombre, " + \
                " apellidos, " + \
                " cedula, " + \
                " celular, " + \
                " mail, " + \
                " contrasena" + \
                " FROM public.usuarios " +\
                " WHERE id_usuario="+str(id)+" ; "
        respuesta = self.my_conn.consultar_db(query)
        return respuesta

    def insertar_usuario(self, nombre, apellidos, cedula, celular, mail, contrasena):
        query = " INSERT INTO public.usuarios " + \
                " (nombre, apellidos, cedula, celular, mail, contrasena) " + \
                "VALUES ('" + nombre + "','" + apellidos + \
                "','" + cedula + "','" + celular + \
                "', '" + mail + "','" + contrasena + "');"
        # print(query)  # for test purposes
        self.my_conn.escribir_db(query)

    def eliminar_usuario(self, id):
        query = "DELETE from public.usuarios where id_usuario = " + \
            str(id)+" ; "
        print(query)
        self.my_conn.escribir_db(query)

    def modificar_usuario(self, id, nombre, apellidos, cedula, celular, mail, contrasena):
        query = " UPDATE public.usuarios " + \
                " SET nombre=" + \
                "'" + nombre + "',apellidos='" + apellidos + \
                "',cedula='" + cedula + "',celular='" + celular + \
                "',mail= '" + mail + "',contrasena='" + \
            contrasena + "' where id_usuario="+id+";"
        self.my_conn.escribir_db(query)


#    def leer_viajes(self):
#        query = "SELECT id, id_pasajero, id_trayecto, inicio, fin" + \
#                " FROM \"Viajes\";"
#        respuesta = self.my_conn.consultar_db(query)  # lista de tuplas
#        return respuesta


    def login(self, user, password):
        query = "SELECT id_usuario, " + \
                " nombre, " + \
                " apellidos, " + \
                " cedula, " + \
                " celular, " + \
                " mail, " + \
                " contrasena" + \
                " FROM public.usuarios " +\
                " WHERE mail='"+user+"' and contrasena='"+password+"' ; "
        respuesta = self.my_conn.consultar_db(query)
        return respuesta

    def cerrar(self):
        self.my_conn.cerrar_db()
